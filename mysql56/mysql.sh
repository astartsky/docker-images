#/bin/bash

if [ ! -f /var/lib/mysql/ibdata1 ]; then

    mysql_install_db

    /usr/bin/mysqld_safe &
    sleep 10s

    echo "CREATE DATABASE $DBNAME"";" | mysql
    echo "GRANT ALL ON *.* TO $DBUSER""@'%' IDENTIFIED BY '$DBPASS""' WITH GRANT OPTION; FLUSH PRIVILEGES" | mysql

    killall mysqld
    sleep 10s
fi

/usr/bin/mysqld_safe --bind-address=0.0.0.0
