CMD="bin/maildev -w 80"

if [ "$WEB_USER" ] && [ "$WEB_PASS" ]; then
  CMD="$CMD --web-user $WEB_USER --web-pass $WEB_PASS"
fi

if [ "$SMTP_PORT" ]; then
  CMD="$CMD --smtp $SMTP_PORT"
fi

exec $CMD
